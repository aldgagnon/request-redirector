const chai = require('chai');
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const index = require('../routes/index.js');

describe('validateParameters', async () => {

    it('should reject if validate_parameters fails when provided invalid parameters', async () => {
        return (expect(index.validateParameters(null)).to.be.rejected
         && expect(index.validateParameters({ host: null})).to.be.rejected
         && expect(index.validateParameters({ host: "", port: null})).to.be.rejected
         && expect(index.validateParameters({ host: "", port: "", path: null})).to.be.rejected
         && expect(index.validateParameters({ host: "", port: "", path: "", method: null})).to.be.rejected
         && expect(index.validateParameters({ host: "", port: "", path: "", method: "", headers: null})).to.be.rejected
         && expect(index.validateParameters({ host: "", port: "", path: "", method: "", headers: "", body: null})).to.be.rejected);
    });
});
