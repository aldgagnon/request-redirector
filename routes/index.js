const express = require('express');
const https = require('https');
const getSpacey = require('get-spacey');
const logger = require('./winston');

const router = express.Router();

// Validate the request parameters
const validateParameters = async (params) => {
  if (params == null) {
    throw new Error('Invalid parameter value.');
  } else if (params.host == null) {
    throw new Error('Invalid parameter host');
  } else if (params.port == null) {
    throw new Error('Invalid parameter port');
  } else if (params.path == null) {
    throw new Error('Invalid parameter path');
  } else if (params.method == null) {
    throw new Error('Invalid parameter method');
  } else if (params.headers == null) {
    throw new Error('invalid parameter headers');
  } else if (params.body == null) {
    throw new Error('Invalid parameter body');
  }
};

// Build and send the request given the request parameters
const sendRequest = async (req) => {
  try {
    await validateParameters(req.body);
  } catch (e) {
    logger.error(`Error validating parameters: ${e.message}`);
    throw e;
  }

  const options = {
    host: req.body.host,
    port: req.body.port,
    path: req.body.path,
    method: req.body.method,
    headers: req.body.headers,
  };

  const { body } = req.body;

  const httpRequest = https.request(options, (res) => {
    logger.debug(`STATUS: ${res.statusCode}`);
    logger.debug(`HEADERS: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      logger.debug(`BODY: ${chunk}`);
    });
  });

  httpRequest.on('error', (e) => {
    logger.info(`Error sending request: ${e.message}`);
    throw new Error(e.message);
  });

  // write data to request body
  httpRequest.write(body);
  httpRequest.end();
};

/* GET method */
router.get('/', (req, res) => {
  res.status(200).end();
});

/* POST method */
router.post('/', (req, res) => {
  sendRequest(req).then(() => {
    res.status(200).end();
  }).catch(() => {
    logger.debug(getSpacey('*** Error sending request ***'));
    res.status(500).end();
  });
});

module.exports = router;
module.exports.validateParameters = validateParameters;
