# request-redirector

A simple HTTP-to-HTTPS redirector. This service takes the parameters from an HTTP POST requests and resend it as an HTTPS POST request. Used as a work around for the lack of HTTPS support on SparkFun's Redboard.

## Installing

1. Clone this repository 
   - `$ git clone https://gitlab.com/aldgagnon/request-redirector.git`
2. Start the Docker container
   - `$ docker-compose up --build -d`

## Usage

An example curl command can be found below.

```
curl -H "Content-Type: application/json" \
--request POST \
--data '{"host":"http:\\api.google.com","port":80,"path":"/target/endpoint/","method":"POST","headers":"Content-Type: application/json", "body":"{\"value\":\"500\"}"}' \
localhost:5000
```
